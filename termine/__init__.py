__version__ = "0.1.5"
__author__ = "Ciaran Farrell"
__author_email__ = 'cfarrell1980@gmail.com'
__url__ = 'http://bitbucket.org/cfarrell1980/termine/'
__long_description__ = '''termine is a commandline tool that makes it easy to connect to
your SOAP enabled Groupwise (r) server (using suds) and to fetch your upcoming appointments.
For example, to list your appointments for the current week, you would use:

	termine list thisweek

and to list your appointments for the current month you would use:

	termine list thismonth

Support for busy searching Groupwise (r) users is present but it is quite rudimentary at the
moment. It is expected that this will be much more visible and usable in future versions.'''

